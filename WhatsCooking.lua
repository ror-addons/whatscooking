WhatsCooking = {}
WBisCasted = false
WBStacks = 0
local version = 1.3
WhatsCooking.BuffIDS = {[3621]=true,[3622]=true,[3623]=true,[3624]=true,[3625]=true,[3626]=true}


function WhatsCooking.Initialize()
CreateWindow("WhatsCookingWindow",false)
LayoutEditor.RegisterWindow( "WhatsCookingWindow", L"WhatsCooking", L"Witchbrew counter", true, true, true, nil )
TextLogAddEntry("Chat", 0, L"<icon57> WhatsCooking "..towstring(version)..L" Loaded.")
RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "WhatsCooking.CheckCast")
RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "WhatsCooking.CheckBuffs")
RegisterEventHandler(TextLogGetUpdateEventId("Combat"), "WhatsCooking.CombatCheck")


local texture, x, y, disabledTexture = GetIconData (10900)
CircleImageSetTexture ("WhatsCookingWindowIcon", texture, 32, 32)
WindowSetTintColor( "WhatsCookingWindowAbilityCooldown", 75,75,75 )

BrewText = string.sub(tostring(GetAbilityName(3621)),1,-4)


end

function WhatsCooking.CheckCast(abilityId)
local CheckAbilityId = abilityId
if abilityId == 9425 then
CooldownDisplaySetCooldown( "WhatsCookingWindowAbilityCooldown", 60, 60 )
WBisCasted = true
WBStacks = tonumber(GetCareerResource( GameData.BuffTargetType.SELF ))+1
end
end

function WhatsCooking.CheckBuffs()
	local WBisACTIVE = false
	local myBuffs = GetBuffs(GameData.BuffTargetType.SELF)
	if( myBuffs == nil ) then return end
	for myBuffId, myBuff in pairs( myBuffs ) do
	local myCurrentBuff = myBuffs[myBuffId]
--	d(myCurrentBuff.abilityId..L" "..myCurrentBuff.name)	
	if WhatsCooking.BuffIDS[myCurrentBuff.abilityId] == true then	 
	WBisACTIVE = true
	WBCD = myCurrentBuff.duration

	
	LabelSetText("WhatsCookingCD",L"("..towstring(WBCD)..L")")
	if WBisCasted == true then
	WBisCasted = false
	LabelSetText("WhatsCookingLabel",towstring(WBStacks))
--	d("OK! "..WBStacks)
	WindowSetShowing("WhatsCookingWindow",true)
	end	
	end		
	end	
	
	if WBisACTIVE == true then
	WindowSetShowing("WhatsCookingWindow",true)
	else
	WindowSetShowing("WhatsCookingWindow",false)
	end
	
end

function WhatsCooking.CombatCheck(updateType, filterType)
if WBStacks > 0 then
WindowSetShowing("WhatsCookingWindow",true)
if ( filterType == ChatSettings.Channels[1002].id) then
local WichMsg
	local num = TextLogGetNumEntries("Combat") - 1 
	_,_,WichMsg = TextLogGetEntry("Combat", num);  
--	if msg:find(BrewText) then
WichMsg = tostring(WichMsg)


if string.find(WichMsg,BrewText) then
	
	WBStacks = WBStacks-1
	LabelSetText("WhatsCookingLabel",towstring(WBStacks))
end
end
else
WindowSetShowing("WhatsCookingWindow",false)
end
end	


