<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="WhatsCooking" version="1.3" date="14/4/2017">
        <Author name="Sullemunk" />
        <Description text="What's Cooking? A Witchbrew stack tracker! By Sullemunk for Oda" />
 
        <Files>
            <File name="WhatsCooking.lua" />
			<File name="WhatsCooking.xml" />
        </Files>

        <OnInitialize>
            <CallFunction name="WhatsCooking.Initialize" />
        </OnInitialize>

		<OnUpdate>
		<CallFunction name="WhatsCooking.CheckBuffs" />
    	</OnUpdate>
		
        <OnShutdown />
    </UiMod>
</ModuleFile>